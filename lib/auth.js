import React, {useState, useEffect, useContext, createContext} from 'react';
import {firebase} from './firebase';
import Router from 'next/router';
import cookie from 'js-cookie';
import {createUser} from './db';

const authContext = createContext();

export function ProvideAuth({children}) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
  return useContext(authContext);
};

function useProvideAuth() {
  const [user, setUser] = useState(null);

  const handleUser = (rawUser) => {
    if (rawUser) {
      const user = formatUser(rawUser);
      const {token, ...userWithoutToken} = user;
      createUser(user.uid, userWithoutToken);
      setUser(user);

      cookie.set('woefwijs-authed', true, {
        expires: 1
      });

      Router.push('/lessons');

      return user;
    } else {
      setUser(false);
      cookie.remove('woefwijs-authed');

      return false;
    }
  };

  const signInWithGoogle = () => {
    try {
      return firebase
        .auth()
        .signInWithPopup(new firebase.auth.GoogleAuthProvider())
        .then((response) => {
          handleUser(response.user);
          return response.user;
        });
    } catch (e) {
      console.error(e)
    }
  };

  const signInWithFacebook = async () => {
    try {
      return firebase
        .auth()
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then((response) => {
          handleUser(response.user);
          return response.user;
        });
    } catch (e) {
      console.error(e)
    }
  };

  const signout = () => {
    Router.push('/');
    return firebase
      .auth()
      .signOut()
      .then(() => handleUser(false));
  };

  const isAdmin = (uid) => {
    switch (uid) {
      case 'diPdX3Ec90QQj8noQUqtmieQsyV2':
        return true;
      case 'VQqgyWgelKPlCmv0cfJoEvSn5X43':
        return true;
      case 'fJm12rZVf3VLzCNyYCYnHnjcTlK2':
        return true;
      case '3jhOaNGmMNMNC4BL91vbFGroIi12':
        return true;
      case 'fOAoCjPz8AXaYF5szLaNhKDk9kI2':
        return true;
      default:
        return false;
    }
  };

  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged(handleUser);
    return () => unsubscribe();
  }, []);

  return {
    user,
    signInWithGoogle,
    signInWithFacebook,
    signout,
    isAdmin
  };
}

const formatUser = (user) => {
  return {
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    token: user.xa || user.ya,
    provider: user.providerData[0].providerId,
    photoUrl: user.photoURL,
    dog: 'Niet ingesteld'
  };
};
