import {db} from './firebase';

export function updateUser(uid, data) {
  return db.collection('users').doc(uid).update(data);
}

export function createUser(uid, data) {
  return db
    .collection('users')
    .doc(uid)
    .set({ uid, ...data }, { merge: true });
}

export function getActiveUser(uid) {
  return db.collection('users').doc(uid).get();
}

export function deleteOlderLesson(uid) {
  return db.collection('lessons').doc(uid).delete();
}
export function createLesson(uid, data) {
  return db
    .collection('lessons')
    .doc(uid)
    .set({ uid, ...data }, { merge: true });
}

export function updateLesson(lesson, data) {
  const { uid, attendees } = lesson;
  return db
    .collection('lessons')
    .doc(uid)
    .update({
      attendees: [...attendees, data.name]
    });
}

export function removeAttendee(lesson, others) {
  const { uid, attendees } = lesson;
  return db.collection('lessons').doc(uid).update({
    attendees: others
  });
}

export async function getAllLessons() {
  const snapshot = await db
    .collection('lessons')
    .orderBy('date')
    .orderBy('order')
    .get();
  const lessons = [];

  snapshot.forEach((doc) => {
    lessons.push({ ...doc.data() });
  });

  return lessons;
}

export async function getLesson(id) {
  const lesson = await db.collection('lessons').doc(id).get();
  return lesson.data();
}
