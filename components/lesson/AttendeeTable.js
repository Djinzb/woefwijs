import React from 'react';
import styles from '../../styles/Home.module.css';

const AttendeeTable = ({ attendees }) => {
  return (
    <div className={styles.lessons}>
      <div className={styles.table}>
        <div className={styles.tableHeader}>
          <span>Naam</span>
        </div>
        {attendees?.map((a,i) => (
          <div className={styles.tableRow} key={`attendee${i}`}>
            <span>{a}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AttendeeTable;
