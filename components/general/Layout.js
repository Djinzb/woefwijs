import React from 'react';
import { useAuth } from '../../lib/auth';
import styles from '../../styles/Home.module.css';
import Navigation from '../navigation';

const Layout = ({ children }) => {
  const { user } = useAuth();
  return (
    <>
      <Navigation user={user} />
      <main className={styles.main}>{children}</main>
      <footer className={styles.footer}>
        <a
          href="https://www.jensbuyst.be/"
          target="_blank"
          rel="noopener noreferrer"
        >
          &#169; Jens Buyst 
        </a>
        <a
          href="https://www.woefwijs.be/"
          target="_blank"
          rel="noopener noreferrer"
        >
        <small style={{fontWeight: '100', marginLeft: '0.3rem'}}>for Woefwijs</small>

        </a>
      </footer>
    </>
  );
};

export default Layout;
