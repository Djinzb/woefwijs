import React from 'react';
import Link from 'next/link';
import Image from 'next/image'
import styles from '../../styles/Nav.module.css';
import { useAuth } from '../../lib/auth';

const index = ({ user }) => {
  const { signout } = useAuth();
  const imageSrc = user?.photoUrl || '/user.png';

  return (
    <nav className={styles.nav}>
      <Link href="/">
        <a>
          <img
              src="/woefwijs.jpg"
              alt="woefwijs logo"
              className={styles.logo}
          />
        </a>
      </Link>
      <h1 className={styles.navHeader}>Inschrijvingstool</h1>
      {user && (
        <>
          <img
            src={imageSrc}
            alt="user profile picture"
            className={styles.profilePic}
          />
          <span>{user?.name}</span>

          <span className={styles.signout} onClick={() => signout()}>
            <svg
              width="22"
              height="23"
              viewBox="0 0 22 23"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M4.6129 2.46885C1.81855 4.52242 -7.49835e-08 7.88114 -7.49835e-08 11.6825C-7.49835e-08 17.9254 4.91451 22.9863 10.9778 23C17.0323 23.0137 21.9911 17.9345 22 11.7008C22.0044 7.89939 20.1859 4.53154 17.396 2.47341C16.877 2.09464 16.154 2.25436 15.8435 2.8248L15.1427 4.10714C14.881 4.5863 15.0052 5.19325 15.4355 5.52182C17.2762 6.92737 18.4516 9.15435 18.4516 11.678C18.456 15.8901 15.1472 19.3492 11 19.3492C6.93709 19.3492 3.52177 15.9631 3.54839 11.6323C3.56169 9.26844 4.64395 6.9867 6.56895 5.51726C6.99919 5.18868 7.11895 4.58174 6.85726 4.10714L6.15645 2.8248C5.84596 2.25893 5.12742 2.09008 4.6129 2.46885ZM9.2258 12.0476V1.09524C9.2258 0.488293 9.7004 0 10.2903 0H11.7097C12.2996 0 12.7742 0.488293 12.7742 1.09524V12.0476C12.7742 12.6545 12.2996 13.1428 11.7097 13.1428H10.2903C9.7004 13.1428 9.2258 12.6545 9.2258 12.0476Z"
                fill="black"
              />
            </svg>
          </span>
        </>
      )}
    </nav>
  );
};

export default index;
