import React from 'react';

const Login = () => {
  return (
    <div className={styles.lessons}>
      <div className={styles.table}>
        <div className={styles.tableHeader}>
          <span>Naam</span>
          <span>Hond</span>
        </div>
        {attendees?.map((a) => (
          <div className={styles.tableRow} key={`attendee${a.uid}`}>
            <span>{a.name}</span>
            <span>{a.dog}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Login;
