import React from 'react';
import styles from '../../styles/Home.module.css';

const SignInButton = ({ method, provider }) => {
  return (
    <div onClick={() => method()} className={styles.signInButton}>
      {getIcon(provider)}
      {provider}
    </div>
  );
};

const getIcon = (provider) => {
  switch (provider) {
    case 'Google':
      return (
        <svg
          width="26"
          height="26"
          viewBox="0 0 26 26"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M26 13.304C26 20.7214 20.8373 26 13.2131 26C5.90328 26 0 20.1919 0 13C0 5.80806 5.90328 0 13.2131 0C16.7721 0 19.7664 1.28427 22.0734 3.40202L18.477 6.80403C13.7725 2.3379 5.02418 5.69274 5.02418 13C5.02418 17.5343 8.70574 21.2089 13.2131 21.2089C18.4451 21.2089 20.4057 17.5185 20.7148 15.6052H13.2131V11.1339H25.7922C25.9148 11.7996 26 12.4391 26 13.304Z"
            fill="black"
          />
        </svg>
      );
    case 'Facebook':
      return (
        <svg
          width="26"
          height="26"
          viewBox="0 0 26 26"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M26 2.78571V23.2143C26 24.7522 24.7522 26 23.2143 26H18.2638V15.7161H21.7808L22.2857 11.7929H18.2638V9.28571C18.2638 8.14821 18.5772 7.37634 20.208 7.37634H22.2857V3.87098C21.9259 3.82455 20.6955 3.71429 19.2562 3.71429C16.2616 3.71429 14.2071 5.54241 14.2071 8.90268V11.7987H10.6786V15.7219H14.2129V26H2.78571C1.24777 26 0 24.7522 0 23.2143V2.78571C0 1.24777 1.24777 0 2.78571 0H23.2143C24.7522 0 26 1.24777 26 2.78571Z"
            fill="black"
          />
        </svg>
      );
    default:
      break;
  }
};

export default SignInButton;
