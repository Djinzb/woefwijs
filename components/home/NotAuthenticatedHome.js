import React from 'react';
import styles from '../../styles/Home.module.css';
import { useAuth } from '../../lib/auth';
import SignInButton from './SignInButton';
import Head from 'next/head';

const NotAuthenticatedHome = () => {
  const { signInWithGoogle, signInWithFacebook } = useAuth();
  
  return (
    <>
    <Head>
      <title>Woefwijs - Aanmelden</title>
    </Head>
      <h1 className={styles.title}>Aanmelden met:</h1>
      <SignInButton
        method={() => signInWithGoogle('/lessons')}
        provider="Google"
      />
      <SignInButton
        method={() => signInWithFacebook('/lessons')}
        provider="Facebook"
      />
    </>
  );
};

export default NotAuthenticatedHome;
