import React from 'react';
import { Form, Field, useForm } from '@leveluptuts/fresh';
import { parseISO } from 'date-fns';
import { mutate } from 'swr';
import styles from '../../styles/Modal.module.css';
import { v4 as uuidv4 } from 'uuid';
import { createLesson } from '../../lib/db';
import { useAuth } from '@/lib/auth';

const ModalAddLesson = ({ setModalOpen }) => {
  const { data } = useForm();
  const auth = useAuth();

  const saveLesson = () => {
    let newLessons = [];
    const lessonGroup = formatLesson(data.lesson);
    lessonGroup.lessons.map((lesson) => {
      const toSave = {
        keyId: uuidv4(),
        addedBy: auth.user.name,
        date: lessonGroup.date,
        class: lesson.class,
        attendees: [],
        order: lesson.order
      };
      createLesson(uuidv4(), toSave);
      newLessons.push(toSave);
    });

    mutate(
      ['/api/lessons', auth.user.token],
      async (res) => ({
        lessons: [...res.lessons, ...newLessons]
      }),
      false
    );

    setModalOpen(false);
  };

  const formatLesson = (lesson) => {
    const lessonGroup = {
      date: parseISO(lesson.datum).toISOString(),
      lessons: []
    };
    if (lesson.puppy) lessonGroup.lessons.push({ class: 'Puppy-klas', order: 0 });
    if (lesson.a) lessonGroup.lessons.push({ class: 'A-klas', order: 3 });
    if (lesson.b) lessonGroup.lessons.push({ class: 'B-klas', order: 2 });
    if (lesson.c) lessonGroup.lessons.push({ class: 'C-klas', order: 1 });
    if (lesson.fun) lessonGroup.lessons.push({ class: 'Fun-klas', order: 4 });
    return lessonGroup;
  };

  return (
    <div className={styles.modalBlur}>
      <div className={styles.modalContainer}>
        <h2>Les toevoegen:</h2>
        <Form
          formId="lesson"
          onSubmit={saveLesson}
          className={styles.form}
          cancelButton={false}
          submitText="Voeg toe"
        >
          <div className={styles.fieldsContainer}>
            <Field type="date" className={styles.input}>
              Datum
            </Field>
            <Field type="toggle" className={styles.toggle}>
              Puppy
            </Field>
            <Field type="toggle" className={styles.toggle}>
              A
            </Field>
            <Field type="toggle" className={styles.toggle}>
              B
            </Field>
            <Field type="toggle" className={styles.toggle}>
              C
            </Field>
            <Field type="toggle" className={styles.toggle}>
              Fun
            </Field>
          </div>
          <CloseButton click={setModalOpen} />
        </Form>
      </div>
    </div>
  );
};

const CloseButton = ({ click }) => (
  <div className={styles.modalClose} onClick={() => click(false)}>
    <svg
      width="13"
      height="13"
      viewBox="0 0 13 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.06287 12.7614L0.23861 10.9371C-0.0795366 10.619 -0.0795366 10.1079 0.23861 9.78977L3.52838 6.5L0.23861 3.21023C-0.0795366 2.89209 -0.0795366 2.38102 0.23861 2.06287L2.06287 0.23861C2.38102 -0.0795366 2.89209 -0.0795366 3.21023 0.23861L6.5 3.52838L9.78977 0.23861C10.1079 -0.0795366 10.619 -0.0795366 10.9371 0.23861L12.7614 2.06287C13.0795 2.38102 13.0795 2.89209 12.7614 3.21023L9.47162 6.5L12.7614 9.78977C13.0795 10.1079 13.0795 10.619 12.7614 10.9371L10.9371 12.7614C10.619 13.0795 10.1079 13.0795 9.78977 12.7614L6.5 9.47162L3.21023 12.7614C2.89547 13.0762 2.38102 13.0762 2.06287 12.7614V12.7614Z"
        fill="black"
      />
    </svg>
  </div>
);

export default ModalAddLesson;
