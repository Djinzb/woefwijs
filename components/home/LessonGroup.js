import { format, parseISO } from 'date-fns';
import { nl } from 'date-fns/locale'
import React from 'react';
import styles from '../../styles/Home.module.css';
import LessonRow from './LessonRow';

const LessonGroup = ({ date, lessons }) => {
  const dateProps = {
    day: format(parseISO(date), 'eeee', { locale: nl }),
    dateDay: format(parseISO(date), 'dd', { locale: nl }),
    dateMonth: format(parseISO(date), 'MMMM', { locale: nl })
  }

  return (
    <div className={styles.tableStriped}>
      <div className={styles.tableRow}>
        <DateComponent {...dateProps} />
      </div>
      {lessons.map((lesson) => (
        <LessonRow les={lesson} key={`lesson${lesson.uid}`} />
      ))}
    </div>
  );
};


const DateComponent = ({ day, dateDay, dateMonth }) => {

  return (
    <span className={styles.dateGroup}>
      <div className={styles.dateContainer}>
        <small >{day}</small>
        <div>{dateDay}</div>
        <small >{dateMonth}</small>
      </div>
    </span>
  )
}
export default LessonGroup;
