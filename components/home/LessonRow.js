import React, { useState, useEffect } from 'react';
import { format, parseISO } from 'date-fns';
import { nl } from 'date-fns/locale'
import NextLink from 'next/link';
import { removeAttendee, updateLesson } from '../../lib/db';
import styles from '../../styles/Home.module.css';
import { useAuth } from '../../lib/auth';

const LessonRow = ({ les }) => {
  const [lesson, setLesson] = useState(les);
  const { user, isAdmin } = useAuth();
  const alreadyAttending = lesson.attendees.filter((a) => a.name === user.name);
  const going = alreadyAttending.length > 0;
  const dayOfWeek = format(parseISO(lesson.date), 'eeee', { locale: nl });

  const renderLessonTime = () => {
    if(lesson.class.toLowerCase() === "puppy-klas") {
      return dayOfWeek === "donderdag" ? "18u30" : "13u30";
    }
    if(lesson.class.toLowerCase() === "c-klas") {
      return dayOfWeek === "donderdag" ? "19u50" : "14u30";
    }
    if(lesson.class.toLowerCase() === "b-klas") {
      return dayOfWeek === "donderdag" ? "19u00" : "16u00";
    }
    if(lesson.class.toLowerCase() === "a-klas") {
      return dayOfWeek === "donderdag" ? "20u40" : "17u15";
    }
    if(lesson.class.toLowerCase() === "fun-klas") {
      return "18u30";
    }
  }
  return isAdmin(user.uid) ? (
    <NextLink href={`/lesson/${lesson.uid}`} passHref key={lesson.uid}>
      <a>
        <div className={`${styles.tableRowInteractive} ${going ? styles.going : ''}`}>
          <span></span>
          <span className={styles.classColumn}>{lesson.class}<small>{renderLessonTime()}</small></span>
          <span>{lesson.attendees.length}</span>
          <span>
            {/* {going && <Attending />}  */}
            <Button lesson={lesson} user={user} setLesson={setLesson} />
          </span>
        </div>
      </a>
    </NextLink>
  ) : (
      <div className={`${styles.tableRow} ${going ? styles.going : ''}`}>
        <span></span>
        <span className={styles.classColumn}>{lesson.class}<small>{renderLessonTime()}</small></span>
        <span>{lesson.attendees.length}</span>
        <span>
          {/* {going && <Attending />} */}
          <Button lesson={lesson} user={user} setLesson={setLesson} />
        </span>
      </div>
    );
};

const Button = ({ lesson, user, setLesson }) => {
  const alreadyAttending = lesson.attendees.filter((l) => l.name === user.name);
  const notGoing = alreadyAttending.length === 0;

  const attendLesson = (e) => {
    e.preventDefault();
    if (alreadyAttending.length <= 0) {
      setLesson({ ...lesson, attendees: [user, ...lesson.attendees] });
      updateLesson(lesson, user);
    }
  };

  const skipLesson = (e) => {
    e.preventDefault();
    const others = lesson.attendees.filter((a) => a.name !== user.name);
    setLesson({ ...lesson, attendees: others });
    removeAttendee(lesson, others);
  };

  return notGoing ? (
    <>
      <button onClick={(e) => attendLesson(e)}>Inschrijven</button>
    </>
  ) : (
      <>
        <div className={styles.skipButtonText}></div>
        <button className={styles.skipButton} onClick={(e) => skipLesson(e)}>
          Uitschrijven
      </button>
      </>
    );
};

const Attending = () => {
  return (
    <div style={{ fontSize: '75%', margin: '0.2rem 0' }}>
      <svg
        width="10"
        height="10"
        viewBox="0 0 10 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M5 0C2.23857 0 0 2.23857 0 5C0 7.76143 2.23857 10 5 10C7.76143 10 10 7.76143 10 5C10 2.23857 7.76143 0 5 0ZM5 0.967742C7.22847 0.967742 9.03226 2.77119 9.03226 5C9.03226 7.22847 7.22881 9.03226 5 9.03226C2.77153 9.03226 0.967742 7.22881 0.967742 5C0.967742 2.77153 2.77119 0.967742 5 0.967742ZM7.82669 3.59409L7.37234 3.13607C7.27825 3.04121 7.12506 3.04058 7.0302 3.1347L4.18036 5.96163L2.97488 4.74637C2.88079 4.65151 2.7276 4.65089 2.63274 4.74498L2.1747 5.19933C2.07984 5.29343 2.07921 5.44661 2.17333 5.54149L4.00359 7.38657C4.09768 7.48143 4.25087 7.48206 4.34573 7.38794L7.82534 3.93625C7.92018 3.84214 7.92079 3.68895 7.82669 3.59409V3.59409Z"
          fill="green"
        />
      </svg>
      Ingeschreven
    </div>
  );
};

export default LessonRow;
