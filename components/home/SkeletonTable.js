import React from 'react'
import styles from '../../styles/Home.module.css';
import "skeleton-screen-css";

const SkeletonTable = () => {
    return (
        <div className={styles.lessons} style={{padding: '1rem 1.5rem'}}>
            <div className="ssc-square mb" style={{ height: '35px', margin: '0.5rem 0', borderRadius: '0.5rem' }}/>
            <div className="ssc-square mb" style={{ height: '35px', margin: '0.5rem 0', borderRadius: '0.5rem' }}/>
            <div className="ssc-square mb" style={{ height: '35px', margin: '0.5rem 0', borderRadius: '0.5rem' }}/>
            <div className="ssc-square mb" style={{ height: '35px', margin: '0.5rem 0', borderRadius: '0.5rem' }}/>
            <div className="ssc-square mb" style={{ height: '35px', margin: '0.5rem 0', borderRadius: '0.5rem' }}/>
        </div>
    )
}

export default SkeletonTable
