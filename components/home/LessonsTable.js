import styles from '../../styles/Home.module.css';
import LessonGroup from './LessonGroup';

const LessonsTable = ({ lessons, groupedLessons }) => {
  
  return (
    <div className={styles.lessons}>
      <div className={styles.table}>
        <div className={styles.tableHeader}>
          <span></span>
          <span>Groep</span>
          <span>Aantal aanwezig</span>
          <span style={{wordBreak: "break-all"}}>Aanwezigheid</span>
        </div>
        {lessons &&
          Object.entries(groupedLessons).map(([key, value]) => (
            <LessonGroup
              date={key}
              lessons={value}
              key={`${key}-${value[0].keyId}`}
            />
          ))}
      </div>
    </div>
  );
};

export default LessonsTable;
