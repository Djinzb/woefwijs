import Head from 'next/head';
import styles from '../styles/Home.module.css';

export default function Home({ lessons }) {
  return (
    <div className={styles.container}>
      <Head>
        <script
          dangerouslySetInnerHTML={{
            __html: `
              if (document.cookie && document.cookie.includes('woefwijs-authed')) {
                window.location.href = "/lessons"
              } else {
                window.location.href = "/login"
              }
            `
          }}
        />
        <title>Woefwijs - Inschrijvingstool</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
    </div>
  );
}
