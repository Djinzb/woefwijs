import Layout from '@/components/general/Layout'
import NotAuthenticatedHome from '@/components/home/NotAuthenticatedHome'
import React from 'react'

const login = () => {
    return (
        <Layout>
            <NotAuthenticatedHome />
        </Layout>
    )
}

export default login
