import { getAllLessons } from '../../lib/db';

export default async (req, res) => {
  try {
    const lessons = await getAllLessons();
    res.status(200).json({ lessons });
  } catch (error) {
    console.log(error);
  }
};
