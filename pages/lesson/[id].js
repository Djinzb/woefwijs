import React from 'react';
import NextLink from 'next/link';
import { getLesson, getAllLessons } from '@/lib/db';
import styles from '@/styles/Home.module.css';
import Layout from '@/components/general/Layout';
import Table from '@/components/lesson/AttendeeTable';
import { format, parseISO } from 'date-fns';

const Lesson = ({ lesson }) => {
  const translate = (date) => {
    if (date === 'Thursday') return 'Donderdag';
    if (date === 'Saturday') return 'Zaterdag';
  };
  return lesson ? (
    <Layout>
      <NextLink href="/lessons">
        <a className={styles.backButton}>
          <BackButton /> Terug
        </a>
      </NextLink>
      <h1 className={styles.title}>
        {translate(format(parseISO(lesson.date), 'eeee'))}
        {format(parseISO(lesson.date), ' - dd/MM/yyyy')} ({lesson.class})
      </h1>
      <div style={{ margin: '1rem 0', fontSize: '1rem' }}>
        Aantal aanwezigen: {lesson.attendees.length}
      </div>
      <Table attendees={lesson.attendees} />
    </Layout>
  ) : (
    <Layout>
      <h2>Er ging iets mis...</h2>
      <NextLink href="/lessons">
        <a className={styles.backButton}>Keer terug</a>
      </NextLink>
    </Layout>
  );
};

export async function getStaticPaths() {
  const allLessons = await getAllLessons();
  const paths = allLessons.map((lesson) => ({
    params: { id: lesson.uid }
  }));

  return { paths, fallback: true };
}

export async function getStaticProps(ctx) {
  const lesson = await getLesson(ctx.params.id);
  return {
    props: {
      lesson
    },
    revalidate: 1
  };
}

const BackButton = () => (
  <svg
    width="10"
    height="16"
    viewBox="0 0 10 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0.340492 7.15027L6.92047 0.35239C7.37527 -0.117463 8.11068 -0.117463 8.56063 0.35239L9.65407 1.48204C10.1089 1.95189 10.1089 2.71165 9.65407 3.17651L4.99486 8L9.65891 12.8185C10.1137 13.2883 10.1137 14.0481 9.65891 14.513L8.56547 15.6476C8.11067 16.1175 7.37527 16.1175 6.92531 15.6476L0.34533 8.84973C-0.114301 8.37988 -0.114301 7.62012 0.340492 7.15027V7.15027Z"
      fill="black"
    />
  </svg>
);

export default Lesson;
