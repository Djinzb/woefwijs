import React, { useState } from 'react';
import useSWR from 'swr';
import { format, parseISO } from 'date-fns';
import styles from '../styles/Home.module.css';
import { useAuth } from '../lib/auth';
import fetcher from '@/utils/fetcher';
import Modal from '@/components/home/ModalAddLesson';
import LessonsTable from '@/components/home/LessonsTable';
import Layout from '@/components/general/Layout';
import Link from 'next/link';
import groupBy from 'lodash.groupby';
import SkeletonTable from '@/components/home/SkeletonTable';
import {deleteOlderLesson} from "@/lib/db";

const LessonsPage = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const { user, isAdmin } = useAuth();

  return (
    <Layout>
      <div className={styles.titleLessons}>
        <h2 className={styles.title}>Lesmomenten</h2>
        {isAdmin(user?.uid) && (
          <button onClick={() => setModalOpen(true)}>Voeg toe</button>
        )}
      </div>
      <Lessons isAdmin={isAdmin(user?.uid)} />
      {modalOpen && <Modal setModalOpen={setModalOpen} />}
    </Layout>
  );
};

const Lessons = ({ isAdmin }) => {
  const { user } = useAuth();
  const { data } = useSWR(user ? ['/api/lessons', user.token] : null, fetcher);

  if (!user) {
    return <Link href="/">Aanmelden</Link>;
  }
  if (!data) {
    return <SkeletonTable />;
  }
  if (data.lessons.length) {
    const today = parseISO(format(new Date(), 'yyyy-MM-dd')).toISOString();
    let filteredLessons = [];
    if(isAdmin) filteredLessons = data.lessons.filter(lesson => lesson.date >= today)
    if(!isAdmin) filteredLessons = data.lessons.filter(lesson => lesson.date > today)

    // console.log(data.lessons.filter(l => l.date <= today))
    // const olderLessons = data.lessons.filter(l => l.date <= today)
    //
    // const removeOlder = () => {
    //   return olderLessons.map(l => deleteOlderLesson(l.uid))
    // }
    return (
      <>
        {/*<button onClick={removeOlder}>remove</button>*/}
      <LessonsTable
        lessons={filteredLessons}
        groupedLessons={groupBy(filteredLessons, 'date')}
        isAdmin={isAdmin}
      />
      </>
    );
  }
  return <div>Geen lessen beschikbaar</div>;
};

export default LessonsPage;
